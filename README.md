# Vehicle Object Detection

A flask project to serve an API that needs an image in a POST request and returns an object detection based on a Darknet and DETR that is trained using the dataset from [Roboflow](https://public.roboflow.com/object-detection/vehicles-openimages) that can detect: 

- Car
- Ambulance
- Bus
- Truck
- Motorcycle

### REQUIREMENTS

- Python 3.6
- [Vortex](https://github.com/nodefluxio/vortex)
- Flask
- OpenCV
- Numpy
- PIL

### HOW TO USE

Clone the project into your local

```
git clone https://gitlab.com/ahmadirfaan_/scene-classification.git
cd scene-classification
```

Run the flask app
```
flask run
```

### HOW TO REQUEST 

You should request a POST into **ipaddress**/detr or **ipaddress**/darknet/img using

| Parameter |    Type   |
| ------    | ------    |
| file      | jpg/png   |

Input example: 

![test](/uploads/0c1225ec8ae2e4d559c5eaa6a114232b/test.jpg)

On Postman: 

![Screenshot_from_2021-09-23_12-15-37](/uploads/3bfde3ca3277eb602fa5a48f9b2f3213/Screenshot_from_2021-09-23_12-15-37.png)

Image output: 

![Screenshot_from_2021-09-23_12-16-15](/uploads/04b3b6c1cafa45fb25da41d5aa1530ea/Screenshot_from_2021-09-23_12-16-15.png)
