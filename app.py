from flask import Flask, request, jsonify, send_file
import os
import cv2
import numpy as np
from vortex.runtime.helper import InferenceHelper
from PIL import Image
import time


#Additional Resource: https://stackoverflow.com/questions/28568687/send-with-multiple-csvs-using-flask

app = Flask(__name__)

kwargs = dict(
        model_path="detr.onnx",
        runtime='cpu',
    )
rt = InferenceHelper.create_runtime_model(**kwargs)

kwargs = dict(
    score_threshold=0.3,
    visualize=True,
)

@app.route("/detr", methods=['POST'])
def predict():

    if('file' not in request.files):
        return jsonify({'msg': 'Check your POST request!'})

    img = request.files['file']
    img = Image.open(img)

    img = np.array(img)
    img = np.expand_dims(img,0)

    size = (480, 640)
    img = np.flip(img,-1)
    img = cv2.resize(img[0],size[::-1])[None,...]

    start = time.time()
    result = rt(img,**kwargs)
    end = time.time()
    print(end-start)

    if 'visualization' in result:
        # visualize first batch
        visual = result['visualization'][0]
        visual = np.flip(visual,2)
        cv2.imwrite("result.jpg", visual)

    return send_file("result.jpg")

@app.route("/darknet/img", methods=['POST'])
def predict_darknet_img():

    if('file' not in request.files):
        return jsonify({'msg': 'Check your POST request!'})

    img = request.files['file']
    img.save("darknet/image.jpg")
    start = time.time()
    os.system("cd darknet && sudo ./darknet detector test data/obj.data yolo-obj.cfg backup/yolo-obj_final.weights image.jpg -dont_show")
    end = time.time()
    print(end-start)

    return send_file("darknet/predictions.jpg")


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
